## Construct and translate Gene Ontology and UBERON relations 

For DataGraph, these import scripts:
- Translate terms such as `obo:BFO_0000050` to `OBIWAN:part_of` for more human-friendly usage in the database. 
- Ontology axioms are extracted in a way that can be handled by OWL-RL.
- Remove unnecessary relations in the ontology files to reduce total database size.

`go_import` is the `robot` command to do the above for GO. 
`uberon_import` is the `robot` command to do the above for UBERON. 

#### Important output files

- `go_import.ttl`
- `uberon_import.ttl`
- `construct_go.ttl`
- `construct_uberon.ttl`
