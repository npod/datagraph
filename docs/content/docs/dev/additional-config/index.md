---
weight: 40
title: "Additional steps"
---

# Checks and additional steps

## Run test queries 
Test queries in `sparql/tests` are run to check ontology inferences.

## Add saved SPARQL queries 
Add saved queries in `sparql/examples` to saved queries panel to get new users started. This can be done manually or through the Workbench API.

## Canned custom views
(As of currently, there are no custom views that need to be set up.)
