---
bookFlatSection: true
title: "Developer Docs"
---

# Developer documentation

**This documentation is for internal development of DataGraph, but it is made open for those in the scientific community interested in reusing, improving upon, and/or integrating with our framework.** DataGraph tries to use open standards and best practices for data sharing. In addition to the more technical materials in this section, developers may still want to review the [User Docs]({{< ref "docs/user/" >}}) as well to get a conceptual feel of the project.

## Section summary

### Components

Knowledgebases are domain-specific. This describes the unique components composing DataGraph.

### Preparing data

Steps for how we process/prepare external ontologies and data files for import.

### Loading data

Steps for configuring repository and importing data.

### Additional steps

Post-data loading steps such as adding examples or custom views.

### Dockerizing

Steps for creating Docker image snapshots.
 
