---
weight: 30
title: "Loading data"
---

# Loading data

There are two methods for configuring the repository and loading data.

{{< hint warning >}}
These steps are specific to our current platform, so additional reference to the technical [GraphDB docs](https://graphdb.ontotext.com/documentation/free/index.html) is also recommended.
{{< /hint >}}

## Method 1 - Configure and load programmatically
1. Make a copy of configuration file `nPOD-config.ttl` and update name and label if necessary.
2. Run script `./load-nPOD-repo`, which uses the configuration file and [LoadRDF tool](http://graphdb.ontotext.com/documentation/standard/loading-data-using-the-loadrdf-tool.html) to create repo and load all data.

{{< hint warning >}}
Expected time for this step is currently 5 hours on a 32GB machine.
{{< /hint >}}

## Method 2 - Configure and load using Workbench (deprecated)
1. Run GraphDB distribution in Standalone Server mode using startup script and access `http://localhost:7200/`.
2. Create new "nPOD-DataGraph-dev" or "nPOD-DataGraph" repository.
3. Configure repository parameters. The parameters that should be changed from default are:
 - Ruleset set to **OWL-RL (Optimized)**. 
 - Un-disable owl:sameAs.
 - Initial entity index size set to 15 million. 
 - Query time-out changed to 600 (10 minutes).
4. Load data files via _Import > RDF_. The recommended order is to load ontology files first, then load instance data.

{{< hint warning >}}
Expected time for this step is currently 7+ hours on a 32GB machine.
{{< /hint >}}

