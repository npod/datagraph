---
weight: 10
title: "Components & platform"
---

# Components

Applications have unique organization and parts, analogous to the unique biological machinery of an organism. This section gives an overview of the important parts and accessories of DataGraph.

## Ontologies

The DataGraph relies on a number of Open Biomedical Ontologies (OBO). 

### External ontologies

- Ontology of Biomedical Investigations (OBI)
- Gene Ontology (GO)
- Cell Ontology (CL)
- UBERON
- Unit Ontology (UO)
- Statistics Ontology (STATO)

### Custom application ontology

- OBI-WAN

## Data

Data are organized at two different levels (which are represented by [templates]({{< relref "#Templates" >}})).

### Dataset


### Feature

## Templates

See [Template Library]({{< ref "templates/0.1" >}}). Templates are used for generating instance data.

## Platform

DataGraph currently runs on [GraphDB Free](https://graphdb.ontotext.com/) v9.3.0. 



