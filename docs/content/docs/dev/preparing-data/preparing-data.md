---
weight: 20
title: "Preparing data"
---

# Preparing data

Some of the external ontologies used in DataGraph should not be imported "as-is"; they require some processing to make them more compatible and usable with a graph database limited to the OWL-RL ruleset profile. All outputs (files to be loaded) are in `imports`.

## Preparing ontology imports
 
1. If updating ontologies, download and replace ontologies with new versions in `imports/ontology`.
2. If updated ontologies are GO or UBERON and need to be reprocessed, run the robot[^robot] scripts in `imports-process/ontology` to regenerate their import versions. Make sure these versions are the ones in `imports/ontology`.


## Preparing instance data
1. Run the `get-data` script in `imports/data`. This will pull the latest instance data from the metadata repository.

## References
[^robot]: robot.obolibrary.org
