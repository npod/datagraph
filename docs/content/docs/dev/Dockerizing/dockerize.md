---
weight: 50
title: "Dockerizing"
---

# Dockerizing

## Access configuration

For (non-dev) Docker images that are published as the webportal.

1. Change admin credentials.
2. Enable "Free Access" in _Setup > Users and Access_. 
3. In the pop-up, enable read-only for the nPOD repository.

## Building Docker image

1. Shut down database and create `.zip` archive. 
2. Copy scripts in `docker` to same location as the `.zip` archive.
3. Do `make nPOD-graphdb`. By default, the image will be tagged with `registry.gitlab.com/npod/datagraph/dev:latest`.
4. Push tagged image `registry.gitlab.com/npod/datagraph/dev:latest` _(dev)_ or `registry.gitlab.com/npod/datagraph:latest` _(prod)_ to Gitlab registry. 
