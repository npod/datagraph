---
weight: 1
bookFlatSection: true
title: "User Docs"
name: user
---

# Introduction

Welcome to the nPOD DataGraph user docs! 

## Main focus

DataGraph preferentially contains knowledge about reusable data that have been vetted, standardized and made accessible. DataGraph *may* also point to some data that isn't easily accessed, but we are cautious about including information for these datasets with doubtful reuse potential. The rationale is that end-users are likely to have a hard time getting to these datasets, therefore it would be misleading/of low utility to include records for these datasets. (Authors who want their data to be reused should make an effort to share using the [FAIR guidelines](https://www.go-fair.org/fair-principles/) that DataGraph uses.) 

## Questions that DataGraph can answer

Researchers can use DataGraph to ask some of the following: 
- What characterization data can I get for alpha cells or dendritic cells or other cell type?
- What data can I get for nPOD donors within certain demographic/clinical parameters? 
- What data exists related to a biological process or research theme?
- What data has been shared by a specific investigator?

Exactly how these questions can be posed and answered are described in more detail in the [usage examples]({{< ref "docs/user/usage" >}}) section.

## History and motivation

The nPOD linked data is of interest to the biology research community, where we have observed sharing of linked data at varying levels: 


1. **PubChem** makes available **RDF data dump files through FTP**, and users have to provide their own triplestore[^pubchem]. This makes the data available but still imposes a burden on the client-side for those who wish to make use of it. This may be fine for clients who are pharmaceutical companies capable of importing PubChem data into their ownl internal knowlegebase, but less so for independent researchers.

2. **Orphanet**[^orphanet] takes a second in-between approach to provide a **Docker image** of the database with the latest ontology and data versions pre-loaded, so that clients can query in an on-demand manner without having to go through the onerous steps of: configuring their own triplestore (if they don't already have one), importing the requisite ontologies, and loading RDF data. The second approach saves a lot of time and makes access vastly easier, but some knowledge of Docker is still required. 

3. **UniProt**[^uniprot] maintains a **SPARQL service** to provide linked data services to the scientific community. This arguably does most to make data accessible/query-able. Examples like UniProt are in the minority because maintaining a high availability endpoint is demanding and expensive due to additional costs of hosting and computing. 


Our goal is to **provide linked data at least at the second level**. One onus that we still cannot completely eliminate for users is that of learning the semantics of our metadata model (ontology). However, our framework is relatively lightweight, mostly re-uses well-known open biomedical ontologies (OBI, Gene Ontology, UBERON, etc.), and comes with documentation.



## References
[^pubchem]: https://pubchemblog.ncbi.nlm.nih.gov/tag/sparql/
[^orphanet]: http://www.orphadata.org/cgi-bin/sparql.html
[^uniprot]: https://sparql.uniprot.org/
