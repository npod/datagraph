---
weight: 10
title: "Overview"
---

# Form and function

{{< hint info >}}
This section extends the [introduction]({{< ref "docs/user/#introduction" >}}) with a conceptual overview that can be skipped for more practical [how-to usage guide]({{< ref "docs/user/usage" >}}).
{{< /hint >}}

For users who want to see the "big picture", here we further describe DataGraph's role, i.e. how DataGraph fits into everything else, analogous to a discussion of a species' particular niche in the ecosystem. 

{{< figure src="nPOD_data_ecosystem.svg" >}}

## Function (purpose)

As show in the figure, DataGraph is part of a larger data application ecosystem. Its role is to:

1. Make data more findable, e.g. through servicing semantic search.
2. Establish persistent and interoperable metadata.


## Form (actualization)

DataGraph comes in different levels of "actualization", e.g. from raw dataset files to the public web interface. In fact, these levels and their representatives were already discussed [in the introduction]({{< ref "docs/user/#history-and-motivation" >}}). 

The functions outlined above depend on the particular form of DataGraph. We give instructions and hints for access and possible use of each form.

{{< figure src="actualization_chart.svg" height="50%" width="50%">}}

### Webportal

{{< hint info >}}
This is considered the most usable form and the main one for finding data (function #1).
{{< /hint >}}

The web interface is the latest version of the DataGraph, made available at this [URL](http://npoddatagraph.westus.azurecontainer.io:7200/). It is the most "evolved" version of DataGraph and allows:
  - Semantic queries with available ontologies
  - Saving and loading queries that can be shared with others
  - Exporting data (but for downloading all data, refer to [data dump]({{< relref "#data-dump" >}}))
  - Programmatic access of the endpoint

Note that it is open to read-only queries but not for data loading. If your use case requires loading custom data, the Docker image would be a good option. 

### Docker image

Releases of the Docker images are essentially snapshots of the knowledgebase. These can be found at https://gitlab.com/npod/datagraph/container_registry. Refer to the [Docker documentation](https://docs.docker.com/engine/) for how to run a container locally. Some differences from the webportal version are:
  - Advanced users can use the `dev` series of images, which allow writing custom data, i.e. if your use case requires loading data
  - Your queries will not be shareable with others

### Data dump

{{< hint info >}}
These data files exist as persistent and interoperable metadata (function #2).
{{< /hint >}}

The RDF datasets are available at the [metadata repository](https://osf.io/8jw52/), each with a URI. The RDF files are stored in two categories: 

1. **dataset** files: A `PMID*.ttl` file contains metadata descriptions for a dataset, most from published papers. Papers can have more than one associated dataset with their own descriptions in separate files, so the format is `PMID*******_1.ttl` for the first dataset and `PMID*******_2.ttl` for the second dataset. Datasets *not* from a published paper have a different naming scheme, e.g. `Core*.ttl`.  

2. **feature** files: A `PMID*.ttl` file contains metadata descriptions for each individual feature in a dataset.  

3. void files: (not yet implemented) These files are the meta files for each of the **dataset** or **feature** files above.






