---
weight: 20
title: "Usage"
name: usage
---

# Usage

This sub-section includes usage examples for querying through the command-line or UI (workbench).

{{< hint info >}}
Want different usage examples? Help us improve by [submitting a suggestion](https://gitlab.com/npod/datagraph/-/issues).
{{< /hint >}}

