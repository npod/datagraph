---
weight: 30
title: "UI query"
---

## Query using SPARQL Panel UI

### Pre-populated examples
The SPARQL panel is pre-populated with examples that can be loaded, as seen in the below screenshot.
{{< figure src="sparql_example.svg" >}}

The examples are meant to reflect the most relevant questions that can be answered by DataGraph, and may change according to knowledgebase updates or user feedback.








