---
weight: 30
title: "Docker (local)"
---

## Docker setup

This section provides instructions for Docker setup.

## Running Image

Make sure you have Docker installed. Pull the image:  
`docker pull registry.gitlab.com/npod/datagraph:latest`

Run image:  
`docker run -p 7200:7200 registry.gitlab.com/npod/datagraph:latest`

In your web browser, navigate to:  
`http://localhost:7200/`






