---
weight: 30
title: "Web instance"
---

## Webportal home

On the home page where repositories are listed, you may be prompted to connect to a repository. Click on `nPOD-dev` to connect. 

You should see the nPOD repository as the "Active repository". On the right-hand side, you should see a panel of "Saved SPARL queries".







