---
menu:
  after:
    name: intro
    weight: 5
title: "Template Library"
---


A collection of draft [Reasonable Ontology Templates (OTTR)](https://ottr.xyz/) to effectively describe how data are captured and instantiated for the DataGraph knowledge base. Templates are implemented for:

> Better abstractions  
> Uniform modelling  
> Modular, encapsulated patterns  
> Separation of design and content  
> Open standards support  
> Publish, share and reuse  
> Tool support for maintenance  

-- see [this resource](https://ottr.xyz/#Benefits) for full discussion and [this resource](https://ottr.xyz/#Specifications) for syntax specifications.


***

### All Templates
