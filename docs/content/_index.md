---
weight: 1
bookFlatSection: true
title: "Intro"
name: intro
---

# Home

## About

DataGraph is a **knowledgebase** that aims to describe all **reusable nPOD-related data** that have ever been produced; nPOD-related data is generally defined as data generated from studies using samples from the [Network of Pancreatic Organ Donors](https://www.jdrfnpod.org/) biobank. DataGraph's implementation is as [linked open data](https://en.wikipedia.org/wiki/Linked_data#Linked_open_data) using [Open Biomedical Ontologies](https://www.ebi.ac.uk/training/online/glossary/open-biomedical-ontologies) and its own custom ontology. DataGraph can help find relevant nPOD data and integrate it with other biological data sources.

For most, we recommended getting started through the [User Docs]({{< ref "docs/user/" >}}). Technical persons interested in the development of DataGraph can also peruse the [Developer Docs]({{< ref "docs/dev/" >}}).

## News

## Reference

Some of this work has been described in:

_(pending reference)_

