Queries in this directory are intended to be example queries for demonstrating usage. Timings and other notes for queries:

| Query                                              | Timing (s)  | Pre-loaded? |  Notes      
|--------------------------------------------------- | ----------- |------------ |---------------------------------
| count_cell_tissue_class                            | 18          |             |
| count_cell_tissue_class_context_vs_natural_context | 26-40       |             | extension of the above
| data_by_GO_annotation                              | <1          | X           |
| data_by_unit_type                                  | <1          | X           |   
| dataset_summary                                    | <1          |             |
| find_cases_matching_criteria                       | <1          | X           |
| find_data_for_specific_caseid                      | <1          | X           |
| Uniprot_federated_query                            | --          | X           |
