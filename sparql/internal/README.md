Queries in this directory represent routine queries used for generating internal statistics and reports. Timings and other notes for queries:

| Query                                              | Timing (s)  |  Notes
|--------------------------------------------------- | ----------- |---------------------------------
| badge_summary                                      | 18          |
| dataset_meta_for_network                           | 26-40       | extension of the above
|                                                    | <1          |

