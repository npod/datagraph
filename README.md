# nPOD DataGraph Pipeline

## General users

See user-facing documentation at https://npod.gitlab.io/datagraph/docs/user.

## Developer/internal use

This repository maintains the pipeline for (A) setting up new versions of the graph knowledgebase and importing data, and (B) creating a Docker image, i.e. the database snapshot. 

Step (A) is usually required only when the underlying ontologies change, and even more rarely when we update to a new version of the database. Typically the pipeline will be referenced at step (B) for creating db snapshots after each new incremental data addition.

The documentation for this pipeline is at https://npod.gitlab.io/datagraph/docs/dev.




