#!/bin/bash

# Convert metadata in .csv format to templated Excel files, which are then expanded to RDF (ttl) instance files

# TEMPLATE can either be "dataset" or "feature", defaulting to the first
# IN_DIR expects input data directory to be available as $PWD/dataset if TEMPLATE=dataset,
# but IN_DIR can also be passed in manually if otherwise.
TEMPLATE=${1:-dataset}
IN_DIR=$PWD/$TEMPLATE
TMP_DIR=xlsx/$TEMPLATE
OUT_DIR=rdf/$TEMPLATE

# $TMP_DIR contains the intermediate templated Excel files
# $OUT_DIR contains the final expanded RDF files
mkdir $TMP_DIR
mkdir $OUT_DIR

Rscript csv_to_template.R $TEMPLATE $IN_DIR $TMP_DIR

for FILE in $TMP_DIR/*; do
  BASEFILE=${FILE##*/}
  java -jar lutra.jar --mode expand -I tabottr --library lib --libraryFormat stottr $FILE -o $OUT_DIR/${BASEFILE%.xlsx}
  echo "processed $OUT_DIR/${BASEFILE%.xlsx}"
done
